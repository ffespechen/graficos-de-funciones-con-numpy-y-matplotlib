import numpy as np
from matplotlib.pyplot import subplot, plot, show, legend, grid

print('Graficando funciones con Numpy y Matplotlib')

print('Funciones lineales...')

varX = np.arange(-50,100,1)
varY1 = varX
varY2 = -varX

#-- Graficamos la función y=x

subplot(2,2,1)
plot(varX, varY1, 'r',label='y=x')
legend(loc='upper center')

#-- graficamos la función y=-x

subplot(2,2,2)
plot(varX, varY2,'g' ,label='y=-x')
legend(loc='upper center')

#-- Graficamos la función y=k (pedimos el un valor al usuario)

print('Vamos a graficar la función y=k')
varK = 100*[int(input('Necesitamos que elijas el valor de k: '))]
varY3 = varK

subplot(2,2,3)
plot(varY3, 'y', label='y=k (donde k='+str(varK[0])+')')
legend(loc='upper center')


#--- Graficamos la función y=mx+b

print('Vamos a graficar la función y=mx+b')
varM = int(input('Elige el valor para la pendiente (m): '))
varB = int(input('Elige el valor para la ordenada al origen (b): '))

varY4 = [(varM*i+varB) for i in varX]

subplot(2,2,4)
plot(varX, varY4, label='y=mx+b (m='+str(varM)+', b='+str(varB)+')')
grid(True)
legend(loc='upper center')



show()
