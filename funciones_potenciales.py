import numpy as np
from matplotlib.pyplot import subplot, plot, show, legend

print('Graficas de funciones potenciales')

#--- Graficando y=x^2

varX = np.arange(-4,4, 0.1)
varY = pow(varX, 2)
subplot(2,2,1)
plot(varX, varY, 'g' , label='y=x²')
legend(loc='upper left')


#--- Graficando y=ax^2 donde a<0
varY1 = [(-2*pow(i, 2)) for i in varX]
subplot(2,2,2)
plot(varX, varY1, 'r', label='y=ax² donde a<0')
legend(loc='upper left')


#--- Graficando y=x^3-8x
varY2 = [(pow(i, 3)-(8*i)) for i in varX]
print(varY2)
subplot(2,2,3)
plot(varX, varY2, 'y', label='y=x³-8x')
legend(loc='upper left')

#--- Graficando y=x⁴-8x²
varY3 = [(pow(i, 4)-(8*pow(i,2))) for i in varX]
subplot(2,2,4)
plot(varX, varY3, label='y=x⁴-8x²')
legend(loc='upper left')

show()
